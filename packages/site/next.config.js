const path = require("path");

module.exports = {
  future: {
    webpack5: true,
  },
  webpack: function (config, { defaultLoaders }) {
    const resolvedBaseUrl = path.resolve(config.context, "../../");
    // This extra config allows to use paths defined in tsconfig
    // rather than next-transpile-modules.
    // @link https://github.com/vercel/next.js/pull/13542
    config.module.rules = [
      ...config.module.rules,
      {
        test: /\.(tsx|ts|js|mjs|jsx)$/,
        include: [resolvedBaseUrl],
        use: defaultLoaders.babel,
        exclude: (excludePath) => {
          return /node_modules/.test(excludePath);
        },
      },
    ];
    return config;
  },
};
